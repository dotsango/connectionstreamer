#include "ByteQueue.hpp"

namespace WolfBlades
{
	uint8 ByteQueue::GetPackageStartFlag() const
	{
		return PackageStartFlag;
	}

	ByteQueue::ByteQueue(const uint32 queueSize, const uint8 packageStartFlag) : LoopQueue(queueSize)
	{
		PackageStartFlag = packageStartFlag;
	}

	bool ByteQueue::PushBytePackage(const BytePackage& package)
	{
		const uint32 package_size = package.GetByteCount();
		if (package_size == 0 || IsMovingRightIndex || GetSpaceLeft() < package_size)
		{
			return false;
		}

		IsMovingRightIndex = true;
		for (uint32 i = 0; i < package_size; i++, MoveIndex(RightIndex))
		{
			QueueHead[RightIndex] = package[i];
			UsedSpace++;
		}
		IsMovingRightIndex = false;
		return true;
	}

	bool ByteQueue::ReadBytePackage(const BytePackage &package, uint32 &bytesIgnored) const
	{
		bytesIgnored = 0;
		const uint32 package_size = package.GetByteCount();
		if (UsedSpace < package_size)
		{
			return false;
		}

		// Ignore bytes to find the start flag
		uint32 index = LeftIndex;
		while (QueueHead[index] != PackageStartFlag && UsedSpace - bytesIgnored >= package_size)
		{
			bytesIgnored++;
			MoveIndex(index);
		}

		if (UsedSpace - bytesIgnored < package_size)
		{
			return false;
		}

		for (uint32 i = 0; i < package_size; i++, MoveIndex(index))
		{
			package[i] = QueueHead[index];
		}

		return true;
	}

	bool ByteQueue::ClearBytes(const uint32 byteCount)
	{
		if (IsMovingLeftIndex)
		{
			return false;
		}

		IsMovingLeftIndex = true;
		if (byteCount >= UsedSpace)
		{
			Clear();
			return true;
		}
		LeftIndex = (LeftIndex + byteCount) % QueueSize;
		UsedSpace -= byteCount;
		IsMovingLeftIndex = false;
		return true;
	}

	ByteQueue& ByteQueue::operator<<(const uint8 byte)
	{
		Push(byte);
		return *this;
	}

	ByteQueue& ByteQueue::operator>>(uint8& byte)
	{
		Pop(byte);
		return *this;
	}

	ByteQueue& ByteQueue::operator<<(const BytePackage& package)
	{
		PushBytePackage(package);
		return *this;
	}

	ByteQueue& ByteQueue::operator>>(const BytePackage& destination)
	{
		uint32 bytes_ignored = 0;
		if (ReadBytePackage(destination, bytes_ignored))
		{
			ClearBytes(bytes_ignored + destination.GetByteCount());
		}
		return *this;
	}
}
