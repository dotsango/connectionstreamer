#include "CommonVideo.hpp"

namespace WolfBlades {
    const std::string &CommonVideo::GetVideoInfo()
    {
        return Info;
    }
    bool CommonVideo::RegisterNewFrameCallback(FrameCallback callback)
    {
        Callbacks.push_back(callback);
        return true;
    }
    bool CommonVideo::GetIsReadingFrames()
    {
        return IsReadingFrames;
    }
    bool CommonVideo::GetIsEndOfStream()
    {
        return IsEndOfStream;
    }
}
