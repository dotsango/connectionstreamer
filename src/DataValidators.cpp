#include "DataValidators.hpp"

constexpr WolfBlades::uint8 CRC8Start = 0xff;
constexpr WolfBlades::uint8 CRC8Table[256] = {
	#include "DATA_CRC8_TABLE.sgd"
};

constexpr WolfBlades::uint16 CRC16Start = 0xffff;
constexpr WolfBlades::uint16 CRC16Table[256] = {
	#include "DATA_CRC16_TABLE.sgd"
};

namespace WolfBlades
{
	namespace DefaultValidators
	{
		bool NotValidateData(const uint8*, uint32)
		{
			return true;
		}

		bool InvalidateAllData(const uint8*, uint32)
		{
			return false;
		}

		uint8 CalculateCRC8(const uint8* byteHead, const uint32 byteCount)
		{
			uint8 crc8 = CRC8Start;
			for (uint32 i = 0; i < byteCount - 1; i++)
			{
				crc8 = CRC8Table[crc8 ^ (byteHead[i])];
			}
			return crc8;
		}

		bool ValidateCRC8(const uint8* byteHead, const uint32 byteCount)
		{
			return CalculateCRC8(byteHead,byteCount) == byteHead[byteCount - 1];
		}

		uint16 CalculateCRC16(const uint8* byteHead, const uint32 byteCount)
		{
			uint16 crc16 = CRC16Start;
			for (uint32 i = 0; i < byteCount - 2; i++)
			{
				crc16 = (crc16 >> 8) ^ CRC16Table[(crc16 ^ static_cast<uint16>(byteHead[i])) & 0x00ff];
			}

			return crc16;
		}

		bool ValidateCRC16(const uint8* byteHead, const uint32 byteCount)
		{
			return CalculateCRC16(byteHead, byteCount) == (*reinterpret_cast<const uint16*>(byteHead+byteCount - 2));
		}
	}
}
