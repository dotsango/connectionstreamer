#include "IOSerialPort.hpp"

namespace WolfBlades
{
	bool IOSerialPort::GetIsConnected() const
	{
		return SerialPortPtr->is_open();
	}

	const std::string& IOSerialPort::GetPortName() const
	{
		return PortName;
	}

	uint32 IOSerialPort::GetBaudRate() const
	{
		return BaudRate;
	}

	IOSerialPort::SerialPortPointer IOSerialPort::GetSerialPortPointer() const
	{
		return SerialPortPtr;
	}

	bool IOSerialPort::RefreshConnection() const
	{
		if (GetIsConnected())
		{
			return true;
		}

		try
		{
			SerialPortPtr->open(PortName);
			SerialPortPtr->set_option(boost::asio::serial_port::baud_rate(BaudRate));
			SerialPortPtr->set_option(boost::asio::serial_port::flow_control(boost::asio::serial_port::flow_control::none));
			SerialPortPtr->set_option(boost::asio::serial_port::parity(boost::asio::serial_port::parity::none));
			SerialPortPtr->set_option(boost::asio::serial_port::stop_bits(boost::asio::serial_port::stop_bits::one));
			SerialPortPtr->set_option(boost::asio::serial_port::character_size(8));
		}
		catch (const char* ex)
		{
			LOG(ERROR) << "Error in refreshing connection: " << ex;
		}
		catch (const std::exception& ex) {
			LOG(ERROR) << "Error in refreshing connection: " << ex.what();
		}

		return GetIsConnected();
	}

	IOSerialPort::IOSerialPort(const std::string& portName, const uint32 baudRate)
	{
		PortName = portName;
		BaudRate = baudRate;
		SerialPortPtr = new SerialPortType(IOService);
	}

	IOSerialPort::IOSerialPort() : IOSerialPort(DefaultPortName, DefaultBaudRate)
	{
		
	}

	IOSerialPort::~IOSerialPort()
	{
		if (SerialPortPtr->is_open())
		{
			SerialPortPtr->close();
		}
		delete SerialPortPtr;
	}

	uint32 IOSerialPort::ReadBytes(const BytePackage& package, const bool autoRefreshConnection) const
	{
		if (autoRefreshConnection)
		{
			for (int i = 0; i < 3 && !RefreshConnection(); i++);
		}

		if (!RefreshConnection())
		{
			LOG(ERROR) << "Failed to connect the serial port(" << PortName << ") after 3 times, give it up before next attempt";
			return 0;
		}

		return read(*SerialPortPtr, 
			boost::asio::buffer(
				package.GetBytesHead(),
				package.GetByteCount()));
	}

	uint32 IOSerialPort::WriteBytes(const BytePackage& package, const bool autoRefreshConnection) const
	{
		if (autoRefreshConnection)
		{
			for (int i = 0; i < 3 && !RefreshConnection(); i++);
		}

		if (!RefreshConnection())
		{
			LOG(ERROR) << "Failed to connect the serial port(" << PortName << ") after 3 times, give it up before next attempt";
			return 0;
		}

		return write(*SerialPortPtr,
			boost::asio::buffer(
				package.GetBytesHead(), 
				package.GetByteCount()));
	}

}
