#include "BytePackageProcessor.hpp"

namespace WolfBlades
{
	void WrapCRC8BytePackage(const BytePackage& source, const BytePackage& destination)
	{
		const uint8* source_head = source.GetBytesHead();
		uint8* destination_head = destination.GetBytesHead();
		destination[0] = '!';
		uint32 i;
		for (i = 0; i < source.GetByteCount(); i++)
		{
			destination_head[i+1] = source_head[i];
		}
		destination_head[i] = DefaultValidators::CalculateCRC8(source_head, source.GetByteCount());
	}

	void ExtractCRC8BytePackage(const BytePackage& source, const BytePackage& destination)
	{
		const uint8* source_head = source.GetBytesHead();
		uint8* destination_head = destination.GetBytesHead();
		for (uint32 i = 0; i < source.GetByteCount() - 2; i++)
		{
			destination_head[i] = source_head[i + 1];
		}
	}

	void WrapCRC16BytePackage(const BytePackage& source, const BytePackage& destination)
	{
		const uint8* source_head = source.GetBytesHead();
		uint8* destination_head = destination.GetBytesHead();
		destination[0] = '!';
		uint32 i;
		for (i = 0; i < source.GetByteCount(); i++)
		{
			destination_head[i+1] = source_head[i];
		}
		*reinterpret_cast<uint16*>(destination_head + i) = DefaultValidators::CalculateCRC16(source_head, source.GetByteCount());
	}

	void ExtractCRC16BytePackage(const BytePackage& source, const BytePackage& destination)
	{
		const uint8* source_head = source.GetBytesHead();
		uint8* destination_head = destination.GetBytesHead();
		for (uint32 i = 0; i < source.GetByteCount() - 3; i++)
		{
			destination_head[i] = source_head[i + 1];
		}
	}
}
