#include "ConnectionFactory.hpp"

namespace WolfBlades
{
	ConnectionFactory::ConnectionFactory(const IOConnector *connector, const BytePackageProcessor wrapper,
										 const BytePackageProcessor extractor, const DataValidator validator, const uint32 sizeofSendPackage,
										 const uint32 sizeofReceivePackage) : ReceiveBytesBuffer(sizeofReceivePackage * 64, 16, '!'),
																			  SendQueue(16),
																			  ReadBuffer(sizeofReceivePackage),
																			  WriteBuffer(sizeofSendPackage),
																			  PopBuffer(sizeofReceivePackage)
	{
		Connector = connector;
		Wrapper = wrapper;
		Extractor = extractor;
		Validator = validator;
		SizeofReceivePackage = sizeofReceivePackage;
		SizeofSendPackage = sizeofSendPackage;
	}

	bool ConnectionFactory::Send(const BytePackage &source)
	{
		Wrapper(source, WriteBuffer);
		const auto package = new BytePackage(WriteBuffer, SizeofSendPackage);
		for (int fail_times = 0; fail_times < 10; fail_times++)
		{
			if (SendQueue.Push(package))
			{
				return true;
			}
		}
		LOG(ERROR) << "Cannot push package into send queue!";
		delete package;
		return false;
	}

	bool ConnectionFactory::Pop(const BytePackage &destination)
	{
		if (!ReceiveBytesBuffer.Pop(PopBuffer))
		{
			return false;
		}

		if (!Validator(PopBuffer.GetBytesHead(), PopBuffer.GetByteCount()))
		{
			LOG(ERROR) << "Received package, but the package is invalid!";
			return false;
		}

		Extractor(PopBuffer, destination);
		return true;
	}

	void ConnectionFactory::DoRoutine(bool *shutdownFlag)
	{
		auto receive_thread = ThreadType(
			[this, shutdownFlag]
			{
			while (!(*shutdownFlag))
			{
				uint32 byte_count = 0;
				try
				{
					byte_count = Connector->ReadBytes(ReadBuffer);
				}
				catch (const char* ex)
				{
					LOG(ERROR) << ex;
				}
				catch (std::exception& ex)
				{
					LOG(ERROR) << ex.what();
				}
				ReceiveBytesBuffer.Push(BytePackage(ReadBuffer.GetBytesHead(), byte_count));
				std::this_thread::sleep_for(1ms);
			} });

		auto send_thread = ThreadType(
			[this, shutdownFlag]
			{
			while (!(*shutdownFlag))
			{
				const BytePackage* p;
				while (!SendQueue.Pop(p)) {
					std::this_thread::sleep_for(1ms);
				}

				if (p == nullptr)
				{
					continue;
				}
				Connector->WriteBytes(*p);
				delete p;
			} });

		receive_thread.join();
		send_thread.join();
	}
}
