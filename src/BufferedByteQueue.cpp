#include "BufferedByteQueue.hpp"

namespace WolfBlades
{
	BufferedByteQueue::BufferedByteQueue(const uint32 queueBytesCount, const uint32 bufferSize,
										 const uint8 packageStartFlag) : InputBuffer(bufferSize),
																		 RawByteQueue(queueBytesCount, packageStartFlag)
	{
	}

	void BufferedByteQueue::Push(const BytePackage &package)
	{
		while (InputBuffer[0] != nullptr)
		{
			if (RawByteQueue.PushBytePackage(*InputBuffer[0]))
			{
				const BytePackage *p;
				InputBuffer.Pop(p);
				delete p;
			}
			else
			{
				break;
			}
		}

		int max_times = 10;
		while (max_times-- > 0)
		{
			if (RawByteQueue.PushBytePackage(package))
			{
				return;
			}
		}

		LOG(WARNING) << "Failed to push bytes into queue";

		const auto *copied_new = new BytePackage(package, package.GetByteCount());
		if (InputBuffer.Push(copied_new))
		{
			return;
		}

		if (InputBuffer.GetQueueSize() != 0)
		{
			LOG(ERROR) << "Buffer of the byte queue overflow! Deleting the first package!";
			const BytePackage *first = nullptr;
			int fail_times = 0;
			while (!InputBuffer.Pop(first))
			{
				if (++fail_times >= 10)
				{
					LOG(ERROR) << "Failed to delete package, give up after 10 attempts";
					return;
				}
			}
			delete first;
			InputBuffer.Push(copied_new);
		}
	}

	BufferedByteQueue &BufferedByteQueue::operator<<(const BytePackage &package)
	{
		Push(package);
		return *this;
	}

	bool BufferedByteQueue::Pop(const BytePackage &package)
	{
		uint32 bytes_ignored = 0;
		if (RawByteQueue.ReadBytePackage(package, bytes_ignored))
		{
			RawByteQueue.ClearBytes(bytes_ignored + package.GetByteCount());
			return true;
		}
		return false;
	}
}
