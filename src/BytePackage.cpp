#include "BytePackage.hpp"

namespace WolfBlades
{
	uint32 BytePackage::GetByteCount() const
	{
		return ByteCount;
	}

	uint8* BytePackage::GetBytesHead() const
	{
		return BytesHead;
	}

	bool BytePackage::GetShouldDeleteBytesHead() const
	{
		return ShouldDeleteBytesHead;
	}

	bool BytePackage::GetCanUseBytes() const
	{
		return CanUseBytes;
	}

	BytePackage::BytePackage(const uint32 byteCount)
	{
		if (byteCount == 0)
		{
			ByteCount = 0;
			BytesHead = nullptr;
			ShouldDeleteBytesHead = false;			//! make this package not usable
			CanUseBytes	= false;
		}
		else
		{
			ByteCount = byteCount;
			BytesHead = new uint8[byteCount]();		//! add "()" to initialize the data
			ShouldDeleteBytesHead = true;
			CanUseBytes	= true;
		}
	}

	BytePackage::BytePackage(uint8* bytesHead, const uint32 byteCount)
	{
		if (byteCount == 0 || bytesHead == nullptr)
		{
			ByteCount = 0;
			BytesHead = nullptr;
			ShouldDeleteBytesHead = false;			//! make this package not usable
			CanUseBytes	= false;
		}
		else
		{
			ByteCount = byteCount;
			BytesHead = bytesHead;
			ShouldDeleteBytesHead = false;
			CanUseBytes = true;
		}
	}

	BytePackage::BytePackage(const BytePackage& other)
	{
		if (this == &other)
		{
			return;
		}

		if (ShouldDeleteBytesHead == true)
		{
			delete[] BytesHead;
		}

		ByteCount = other.ByteCount;
		BytesHead = other.BytesHead;
		ShouldDeleteBytesHead = false;
		CanUseBytes = true;
	}

	BytePackage::BytePackage(const BytePackage& other, const uint32 newByteCount) : BytePackage(newByteCount)
	{
		if (other.CanUseBytes)
		{
			//这里一定会返回 true 才对
			if (!ImportBytes(other.BytesHead, newByteCount > other.ByteCount ? other.ByteCount : newByteCount, false))
			{
				throw "Invalid status when import bytes(impossible status!!)";
			}
		}
	}

	BytePackage::BytePackage() : BytePackage(0)
	{
	}

	BytePackage::~BytePackage()
	{
		ByteCount = 0;
		if (ShouldDeleteBytesHead)
		{
			delete[] BytesHead;
		}
	}

	uint8& BytePackage::operator[](const uint32 index) const
	{
		return BytesHead[index];
	}

	bool BytePackage::ImportBytes(const uint8* sourceBytesHead, const uint32 sourceByteCount, const bool stopWhenFull) const
	{
		if (!CanUseBytes)
		{
			return false;
		}

		if (sourceByteCount > ByteCount)
		{
			memcpy(BytesHead, sourceBytesHead, ByteCount);
			return stopWhenFull;
		}

		memcpy(BytesHead, sourceBytesHead, sourceByteCount);
		return true;
	}

	bool BytePackage::ExportBytes(const uint8* destinationBytesHead, const uint32 destinationByteCount, const bool stopWhenFull) const
	{
		if (!CanUseBytes)
		{
			return false;
		}

		if (ByteCount > destinationByteCount)
		{
			memcpy(BytesHead, destinationBytesHead, destinationByteCount);
			return stopWhenFull;
		}

		memcpy(BytesHead, destinationBytesHead, ByteCount);
		return true;
	}

	void BytePackage::ClearBytes() const
	{
		if (!CanUseBytes)
		{
			return;
		}

		memset(BytesHead, 0, ByteCount);
	}

	bool BytePackage::operator==(const BytePackage& other) const
	{
		return BytesHead == other.BytesHead;
	}
}
