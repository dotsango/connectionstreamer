#include "IOSocket.hpp"

WolfBlades::IOSocket::IOSocket(SocketType type, const std::string& target, const unsigned short port)
{
	Target = target;
	Port = port;
	switch (type)
	{
	case Unknown:
		UnionSocketPtr = nullptr;
		break;
	case TCP:
		UnionSocketPtr = new UnionSocket;
		UnionSocketPtr->Type = type;
		UnionSocketPtr->SelectSocketPointer.TCP = new TCPSocket(IOService);
		break;

	case UDP:
		UnionSocketPtr = new UnionSocket;
		UnionSocketPtr->Type = type;
		UnionSocketPtr->SelectSocketPointer.UDP = new UDPSocket(IOService);
		break;
	default:
		UnionSocketPtr = nullptr;
		break;
	}
}

WolfBlades::IOSocket::~IOSocket()
{
	if (UnionSocketPtr == nullptr || UnionSocketPtr->Type == Unknown)
	{
		return;
	}

	if (UnionSocketPtr->Type == TCP)
	{
		if (UnionSocketPtr->SelectSocketPointer.TCP->is_open())
		{
			UnionSocketPtr->SelectSocketPointer.TCP->close();
			delete UnionSocketPtr->SelectSocketPointer.TCP;
		}
	}
	else
	{
		if (UnionSocketPtr->SelectSocketPointer.UDP->is_open())
		{
			UnionSocketPtr->SelectSocketPointer.UDP->close();
			delete UnionSocketPtr->SelectSocketPointer.UDP;
		}
	}

	delete UnionSocketPtr;
}

bool WolfBlades::IOSocket::GetIsConnected() const
{
	if (UnionSocketPtr == nullptr)
	{
		return false;
	}

	switch (UnionSocketPtr->Type)
	{
	case Unknown:
		return false;

	case TCP:
		return UnionSocketPtr->SelectSocketPointer.TCP->is_open();

	case UDP:
		return UnionSocketPtr->SelectSocketPointer.UDP->is_open();

	default:
		return false;
	}
}

const WolfBlades::IOSocket::UnionSocket* WolfBlades::IOSocket::GetUnionSocketPointer() const
{
	return UnionSocketPtr;
}

const std::string& WolfBlades::IOSocket::GetConnectTarget() const
{
	return Target;
}

unsigned short WolfBlades::IOSocket::GetPort() const
{
	return Port;
}

bool WolfBlades::IOSocket::RefreshConnection() const
{
	if (GetIsConnected())
	{
		return true;
	}

	if (UnionSocketPtr == nullptr || UnionSocketPtr->Type == Unknown)
	{
		return false;
	}

	try
	{
		if (UnionSocketPtr->Type == TCP)
		{
			const boost::asio::ip::tcp::endpoint tcp_endpoint(boost::asio::ip::address::from_string(Target), Port);
			UnionSocketPtr->SelectSocketPointer.TCP->connect(tcp_endpoint);
		}
		else
		{
			const boost::asio::ip::udp::endpoint udp_endpoint(boost::asio::ip::address::from_string(Target), Port);
			UnionSocketPtr->SelectSocketPointer.UDP->connect(udp_endpoint);
		}
	}
	catch (const char* ex)
	{
		LOG(ERROR) << "Error in refreshing connection: " << ex;
	}
	catch (const std::exception& ex)
	{
		LOG(ERROR) << "Error in refreshing connection: " << ex.what();
	}

	return GetIsConnected();
}

WolfBlades::uint32 WolfBlades::IOSocket::ReadBytes(const BytePackage& package, const bool autoRefreshConnection) const
{
	if (autoRefreshConnection)
	{
		for (int i = 0; i < 3 && !RefreshConnection(); i++);
	}

	if (!RefreshConnection())
	{
		LOG(ERROR) << "Failed to connect the EndPoint(" << Target << ":" << Port <<
			") after 3 times, give it up before next attempt";
		return 0;
	}

	return UnionSocketPtr->Type == TCP
		       ? UnionSocketPtr->SelectSocketPointer.TCP->receive(
			       boost::asio::buffer(
				       package.GetBytesHead(),
				       package.GetByteCount()))
		       : UnionSocketPtr->SelectSocketPointer.UDP->receive(
			       boost::asio::buffer(
				       package.GetBytesHead(),
				       package.GetByteCount()));
}

WolfBlades::uint32 WolfBlades::IOSocket::WriteBytes(const BytePackage& package, const bool autoRefreshConnection) const
{
	if (autoRefreshConnection)
	{
		for (int i = 0; i < 3 && !RefreshConnection(); i++);
	}

	if (!RefreshConnection())
	{
		LOG(ERROR) << "Failed to connect the EndPoint(" << Target << ":" << Port <<
			") after 3 times, give it up before next attempt";
		return 0;
	}

	return UnionSocketPtr->Type == TCP
		       ? UnionSocketPtr->SelectSocketPointer.TCP->send(
			       boost::asio::buffer(
				       package.GetBytesHead(),
				       package.GetByteCount()))
		       : UnionSocketPtr->SelectSocketPointer.UDP->send(
			       boost::asio::buffer(
				       package.GetBytesHead(),
				       package.GetByteCount()));
}
