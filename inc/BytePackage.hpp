#pragma once

#include <exception>
#include <cstring>
#include "MemoryTypes.hpp"

namespace WolfBlades
{
	/**
	 * \brief 代表以字节为基础单位的包裹
	 */
	class BytePackage
	{
	public:
		/**
		 * \brief 代表一个字节
		 */
		using byte = unsigned char;

	private:
		/**
		 * \brief 代表数据的开头
		 */
		uint8* BytesHead;

		/**
		 * \brief 代表数据的长度（字节数目）
		 */
		uint32 ByteCount;

		/**
		 * \brief 指示当前包裹销毁时是否应该销毁数据头（也就是包裹内的数据头是否属于自己）
		 */
		bool ShouldDeleteBytesHead;

		/**
		 * \brief 指示当前包裹是否能够修改数据包裹
		 */
		bool CanUseBytes;

	public:
		/**
		 * \brief 获取包裹空间的大小
		 * \return 字节数
		 */
		uint32 GetByteCount() const;

		/**
		 * \brief 获取包裹空间的数据头
		 * \return 数据头指针
		 */
		uint8* GetBytesHead() const;

		/**
		 * \brief 获取此包裹是否应该销毁内存（如果应该销毁，那么说明此包裹的内存属于自己）
		 * \return 是否应该销毁内存
		 */
		bool GetShouldDeleteBytesHead() const;

		/**
		 * \brief 获取此包裹是否能够写入或者提取字节
		 * \return 是否能够写入或者提取字节
		 */
		bool GetCanUseBytes() const;

		/**
		 * \brief 构造一个指定大小的新包裹，此包裹销毁时销毁空间
		 * \param byteCount 新包裹能够存储的字节数
		 */
		BytePackage(uint32 byteCount);

		/**
		 * \brief 构造一个新包裹，内部的空间和字节数都来自于指定参数，此包裹销毁时不销毁空间
		 * \param bytesHead 指定空间的开头
		 * \param byteCount 指定空间的大小（字节数）
		 */
		BytePackage(uint8* bytesHead, uint32 byteCount);

		/**
		 * \brief 复制构造一个新包裹，内部的空间和字节数来自于指定包裹，此包裹销毁时不销毁空间
		 * \param other 要复制的包裹
		 */
		BytePackage(const BytePackage& other);

		/**
		 * \brief 复制构造一个真正的新包裹，指定字节数，创建新的内部的空间（如果可以，复制other包裹内部空间的所有内存），此包裹销毁时销毁空间
		 * \param other 要复制的包裹
		 * \param newByteCount 指定新的内部空间的大小（字节数）
		 */
		BytePackage(const BytePackage& other, uint32 newByteCount);

		/**
		 * \brief 构造一个不能用的空包裹，此包裹销毁时不销毁空间
		 */
		BytePackage();

		/**
		 * \brief 尝试销毁包裹的内部空间
		 */
		~BytePackage();

		/**
		 * \brief 提供快速访问数组的方式
		 * \param index 下标
		 * \return 字节
		 */
		uint8& operator[](uint32 index) const;

		/**
		 * \brief 写入字节
		 * \param sourceBytesHead 要写入的字节的开头 
		 * \param sourceByteCount 要写入的字节的数目
		 * \param stopWhenFull 如果写入的字节过多，在包裹满的时候停下
		 * \return 如果所有字节都能写入，或者在指定stopWhenFull时包裹写满，那么返回true，否则返回false
		 */
		bool ImportBytes(const uint8* sourceBytesHead, uint32 sourceByteCount, bool stopWhenFull) const;

		/**
		 * \brief 提取字节
		 * \param destinationBytesHead 要存放的字节的开头
		 * \param destinationByteCount 要存放的字节的数目
		 * \param stopWhenFull 如果存放的字节过多，在存放空间慢的时候停下
		 * \return 如果所有字节都能写入，或者在指定stopWhenFull时存放空间写满，那么返回true，否则返回false
		 */
		bool ExportBytes(const uint8* destinationBytesHead, uint32 destinationByteCount, bool stopWhenFull) const;

		/**
		 * \brief 清理空间为全默认0
		 */
		void ClearBytes() const;

		/**
		 * \brief 判断内部的空间是否相同（不在乎内部空间大小，只在乎头是否相同）
		 * \param other 要比较的包裹
		 * \return 内部空间的数据头是否相同
		 */
		bool operator==(const BytePackage& other) const;
	};
}
