#pragma once

#include <boost/thread.hpp>
#include <thread>
#include <chrono>

namespace WolfBlades
{
	using namespace std::chrono_literals;

	/**
	 * \brief Boost线程类型
	 */
	typedef boost::thread ThreadType;
}
