#pragma once

#include "CommonVideo.hpp"

namespace WolfBlades {
    class OpenCVVideo final : public CommonVideo {
    private:
        cv::VideoCapture Video;
    public:
        bool FindVideo(std::string info) override;
        bool OpenVideoStream() override;
        bool CloseVideoStream() override;
    };
}
