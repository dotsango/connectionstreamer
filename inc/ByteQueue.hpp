#pragma once

#include <glog/logging.h>
#include "LoopQueue.hpp"
#include "BytePackage.hpp"

namespace WolfBlades
{
	/**
	 * \brief 代表字节循环队列
	 */
	class ByteQueue : public LoopQueue<uint8>
	{
	protected:
		/**
		 * \brief 表示包裹的起始标志，一般设置为'!'
		 */
		uint8 PackageStartFlag;
	public:
		/**
		 * \brief 获取包的开头字节（在ReadBytePackage时保证每个包都以它开头）
		 * \return 包的开头字节
		 */
		uint8 GetPackageStartFlag() const;

		/**
		 * \brief 构造一个字节循环队列
		 * \param queueSize 循环队列字节数目
		 * \param packageStartFlag 包的开头字节
		 */
		ByteQueue(uint32 queueSize, uint8 packageStartFlag);

		/**
		 * \brief 压入一个包裹
		 * \param package 要压入的包裹
		 * \return 操作是否成功
		 */
		bool PushBytePackage(const BytePackage &package);

		/**
		 * \brief 读取一个包裹（注意，读取的字节不会被移除）
		 * \param package 读取的包裹存放的位置
		 * \param bytesIgnored 因为读取包裹而忽略的字节数
		 * \return 操作是否成功
		 */
		bool ReadBytePackage(const BytePackage &package, uint32 &bytesIgnored) const;

		/**
		 * \brief 从队列中移除指定数量的字节
		 * \param byteCount 字节数
		 * \return 操作是否成功
		 */
		bool ClearBytes(uint32 byteCount);

		/**
		 * \brief 重载字节输入运算法
		 * \param byte 要压入的字节
		 * \return 当前实例，方便链式表达
		 */
		ByteQueue& operator<<(uint8 byte);

		/**
		 * \brief 重载字节输出运算法
		 * \param byte 要放出的字节的存放位置
		 * \return 当前实例，方便链式表达
		 */
		ByteQueue& operator>>(uint8 &byte);

		/**
		 * \brief 调用PushBytePackage压入包裹（只在你确定这个队列不被其他线程占用的时候调用，即PushBytePackage操作一定能成功才可）
		 * \param package 要压入的包裹
		 * \return 当前实例，方便链式表达
		 */
		ByteQueue& operator<<(const BytePackage& package);

		/**
		 * \brief 调用ReadBytePackage压入包裹（只在你确定这个队列不被其他线程占用的时候调用，即ReadBytePackage操作一定能成功才可）
		 * \param destination 读取的包裹存放的位置
		 * \return 当前实例，方便链式表达
		 */
		ByteQueue& operator>>(const BytePackage& destination);
	};
}
