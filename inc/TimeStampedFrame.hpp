#pragma once

#include <opencv4/opencv2/opencv.hpp>

namespace WolfBlades
{
    struct TimeStampedFrame
    {
        cv::Mat Frame;
        std::chrono::steady_clock::time_point TimeStamp;
    };
}
