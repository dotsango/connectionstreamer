#pragma once

#include "IOConnector.hpp"

namespace WolfBlades
{
	/**
	* \brief 默认的波特率
	*/
	constexpr auto DefaultBaudRate = 115200;

	/**
	* \brief 默认的串口名称
	*/
	constexpr auto DefaultPortName = "COM0";

	/**
	 * \brief 代表一个串口（使用时必须立即指定串口名称和波特率）
	 */
	class IOSerialPort final : public IOConnector
	{
	public:
		typedef boost::asio::serial_port SerialPortType, *SerialPortPointer;
		typedef boost::asio::io_service IOServiceType;

	private:
		/**
		 * \brief 串口指针，随此类销毁
		 */
		SerialPortPointer SerialPortPtr;

		/**
		 * \brief IO服务，随此类销毁
		 */
		IOServiceType	  IOService;

		/**
		 * \brief 指定的串口名称
		 */
		std::string		  PortName;

		/**
		 * \brief 指定的波特率
		 */
		uint32			  BaudRate;

	public:
		/**
		 * \brief 获取串口是否连接上
		 * \return 串口是否连接上
		 */
		bool GetIsConnected() const;

		/**
		 * \brief 获取串口名称
		 * \return 串口名称
		 */
		const std::string& GetPortName() const;

		/**
		 * \brief 获取波特率
		 * \return 波特率
		 */
		uint32 GetBaudRate() const;

		/**
		 * \brief 获取内部的串口指针
		 * \return 串口指针
		 */
		SerialPortPointer GetSerialPortPointer() const;

		/**
		 * \brief 刷新串口的连接
		 * \return 串口是否成功连接上
		 */
		bool RefreshConnection() const;

		/**
		 * \brief 创建一个串口
		 * \param portName 指定串口名称
		 * \param baudRate 指定串口波特率
		 */
		IOSerialPort(const std::string &portName, uint32 baudRate);

		/**
		 * \brief 创建一个默认的串口（使用默认的串口名称和默认的波特率），但不连接
		 */
		IOSerialPort();

		/**
		 * \brief 关闭串口，释放资源
		 */
		~IOSerialPort();

		/**
		 * \brief 从串口中读取数据到包裹中，此时包裹不可被其他进程使用
		 * \param package 数据包裹
		 * \param autoRefreshConnection 是否自动刷新连接
		 * \return 读取到的数据数量（如果连接断开，那么返回0）
		 */
		uint32 ReadBytes(const BytePackage& package, bool autoRefreshConnection = true)	const override;

		/**
		 * \brief 从包裹中写入数据到串口中，此时包裹可以被其他进程使用，但是内部空间不可被销毁
		 * \param package 数据包裹
		 * \param autoRefreshConnection 是否自动刷新连接
		 * \return 写入的数据数量（如果连接断开，那么返回0）
		 */
		uint32 WriteBytes(const BytePackage& package,bool autoRefreshConnection = true)	const override;
	};
}