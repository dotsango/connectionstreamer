#pragma once

#include <string>
#include "TimeStampedFrame.hpp"
#include <vector>

namespace WolfBlades {
    class CommonVideo {
    public:
        typedef void (*FrameCallback)(const TimeStampedFrame& frame);

    protected:
        volatile bool IsReadingFrames;
        volatile bool IsEndOfStream;
        std::string Info;
        std::vector<FrameCallback> Callbacks;

    public:
        virtual bool FindVideo(std::string info) = 0;

        const std::string& GetVideoInfo();

        bool RegisterNewFrameCallback(FrameCallback callback);

        virtual bool OpenVideoStream() = 0;

        virtual bool CloseVideoStream() = 0;

        bool GetIsReadingFrames();

        bool GetIsEndOfStream();
    };
}