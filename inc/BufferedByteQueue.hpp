#pragma once

#include "ByteQueue.hpp"
#include <glog/logging.h>

namespace WolfBlades
{
	/**
	 * \brief 代表一个有缓冲的字节循环队列（Push操作一定能成功的队列）
	 */
	class BufferedByteQueue
	{
	public:
		/**
		 * \brief 代表一个包裹队列
		 */
		typedef LoopQueue<const BytePackage*> BytePackageQueue;

	private:
		/**
		 * \brief 输入包裹的缓冲区
		 */
		BytePackageQueue InputBuffer;

		/**
		 * \brief 字节队列，用来储存输入字节和输出包裹
		 */
		ByteQueue RawByteQueue;

	public:
		/**
		 * \brief 构造一个缓冲字节循环队列
		 * \param queueBytesCount 队列大小（字节数）
		 * \param bufferSize 缓冲区大小（包裹数）
		 * \param packageStartFlag 包裹的开头
		 */
		BufferedByteQueue(uint32 queueBytesCount, uint32 bufferSize, uint8 packageStartFlag);

		/**
		 * \brief 压入包裹中的数据（不负责delete包裹）（注意，不返回是否成功，因为包裹一定能压进去）
		 * \param package 要压入的包裹
		 */
		void Push(const BytePackage& package);

		/**
		 * \brief 提供以左移运算符压入包裹的代码格式
		 * \param package 要压入的包裹
		 * \return 返回当前实例以实现链式表达
		 */
		BufferedByteQueue& operator<<(const BytePackage& package);

		/**
		 * \brief 放出包裹
		 * \param package 要放出的包裹的位置
		 * \return 操作是否成功
		 */
		bool Pop(const BytePackage& package);
	};
}
