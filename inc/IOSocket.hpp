#pragma once

#include "IOConnector.hpp"

namespace WolfBlades
{
	/**
	 * \brief 代表一个Socket，可以是UDPSocket或TCPSocket
	 */
	class IOSocket final : public IOConnector
	{
	public:
		/**
		 * \brief 表明Socket的具体类型
		 */
		enum SocketType
		{
			/**
			 * \brief 未知类型
			 */
			Unknown = 0,

			/**
			 * \brief TCPSocket
			 */
			TCP = 1,

			/**
			 * \brief UDPSocket
			 */
			UDP = 2
		};

		typedef boost::asio::ip::tcp::socket TCPSocket, *TCPSocketPointer;
		typedef boost::asio::ip::udp::socket UDPSocket, *UDPSocketPointer;

		/**
		 * \brief Socket指针联合体
		 */
		struct UnionSocket
		{
			/**
			 * \brief 指明Socket类型
			 */
			SocketType Type;

			/**
			 * \brief 存储Socket指针
			 */
			union
			{
				/**
				 * \brief TCPSocket指针
				 */
				TCPSocketPointer TCP;

				/**
				 * \brief UDPSocket指针
				 */
				UDPSocketPointer UDP;
			} SelectSocketPointer;
		};

		typedef boost::asio::io_service IOServiceType;

	private:
		/**
		 * \brief Socket指针，随此类销毁
		 */
		UnionSocket* UnionSocketPtr;

		/**
		 * \brief IO服务，随此类销毁
		 */
		IOServiceType IOService;

		/**
		 * \brief 要连接的目标ip地址
		 */
		std::string Target;

		/**
		 * \brief 要访问的目标端口
		 */
		unsigned short Port;

	public:
		/**
		 * \brief 提供Socket类型还有要连接的目标来构造Socket
		 * \param type Socket类型
		 * \param target 要连接的目标ip地址
		 * \param port 要访问的端口
		 */
		IOSocket(SocketType type, const std::string& target, unsigned short port);

		/**
		 * \brief 关闭Socket，释放资源
		 */
		virtual ~IOSocket();

		/**
		 * \brief 获取Socket是否连接上
		 * \return Socket是否连接上
		 */
		bool GetIsConnected() const;

		/**
		 * \brief 获取内部的Socket指针
		 * \return Socket指针
		 */
		const UnionSocket* GetUnionSocketPointer() const;

		/**
		 * \brief 获取要连接的目标ip地址
		 * \return 要连接的目标
		 */
		const std::string& GetConnectTarget() const;

		/**
		 * \brief 获取要访问的目标端口
		 * \return 要访问的目标端口
		 */
		unsigned short GetPort() const;

		/**
		 * \brief 刷新Socket的连接
		 * \return Socket是否成功连接上
		 */
		bool RefreshConnection() const;

		/**
		 * \brief 从Socket中读取数据到包裹中，此时包裹不可被其他进程使用
		 * \param package 数据包裹
		 * \param autoRefreshConnection 是否自动刷新连接
		 * \return 读取到的数据数量（如果连接断开，那么返回0）
		 */
		uint32 ReadBytes(const BytePackage& package, bool autoRefreshConnection = true)	const override;

		/**
		 * \brief 从包裹中写入数据到Socket中，此时包裹可以被其他进程使用，但是内部空间不可被销毁
		 * \param package 数据包裹
		 * \param autoRefreshConnection 是否自动刷新连接
		 * \return 写入的数据数量（如果连接断开，那么返回0）
		 */
		uint32 WriteBytes(const BytePackage& package,bool autoRefreshConnection = true)	const override;
	};
}
