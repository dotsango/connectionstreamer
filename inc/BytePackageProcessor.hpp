#pragma once
#include "BytePackage.hpp"
#include "DataValidators.hpp"

namespace WolfBlades
{
	/**
	 * \brief 处理包裹并产生一个新的包裹
	 */
	typedef void (*BytePackageProcessor)(const BytePackage& source, const BytePackage& destination);

	/**
	 * \brief 为包裹添加帧头'!'和尾部CRC8
	 * \param source 要读取的数据源
	 * \param destination 要写入的数据位置
	 */
	void WrapCRC8BytePackage(const BytePackage& source, const BytePackage& destination);

	/**
	 * \brief 为包裹去除帧头'!'和尾部CRC8
	 * \param source 要读取的数据源
	 * \param destination 要写入的数据位置
	 */
	void ExtractCRC8BytePackage(const BytePackage& source, const BytePackage& destination);

	/**
	 * \brief 为包裹添加帧头'!'和尾部CRC16
	 * \param source 要读取的数据源
	 * \param destination 要写入的数据位置
	 */
	void WrapCRC16BytePackage(const BytePackage& source, const BytePackage& destination);

	/**
	 * \brief 为包裹去除帧头'!'和尾部CRC16
	 * \param source 要读取的数据源
	 * \param destination 要写入的数据位置
	 */
	void ExtractCRC16BytePackage(const BytePackage& source, const BytePackage& destination);
}
