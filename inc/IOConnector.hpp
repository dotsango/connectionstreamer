#pragma once

#include <boost/asio.hpp>
#include <string>
#include "BytePackage.hpp"
#include "GLogHolder.hpp"

namespace WolfBlades
{
	/**
	 * \brief 代表IO连接器，可以是SerialPort TCPSocket UDPSocket中的一种
	 */
	class IOConnector
	{
	protected:
		~IOConnector() = default;

	public:
		/**
		 * \brief 从串口中读取数据到包裹中，此时包裹不可被其他进程使用
		 * \param package 数据包裹
		 * \param autoRefreshConnection 是否自动刷新连接
		 * \return 读取到的数据数量（如果连接断开，那么返回0）
		 */
		virtual uint32 ReadBytes(const BytePackage& package, bool autoRefreshConnection = true)	const = 0;

		/**
		 * \brief 从包裹中写入数据到串口中，此时包裹可以被其他进程使用，但是内部空间不可被销毁
		 * \param package 数据包裹
		 * \param autoRefreshConnection 是否自动刷新连接
		 * \return 写入的数据数量（如果连接断开，那么返回0）
		 */
		virtual  uint32 WriteBytes(const BytePackage& package,bool autoRefreshConnection = true) const = 0;
	};
}
