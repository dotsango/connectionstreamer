#pragma once

#include "MemoryTypes.hpp"

namespace WolfBlades
{
	/**
	 * \brief 代表数据验证器函数
	 * \param byteHead 表示要验证的数据的开头
	 * \param byteCount 表示要验证的数据的体积（字节数）
	 * \return 是否通过验证
	 */
	typedef bool (*DataValidator)(const uint8 *byteHead, uint32 byteCount);

	/**
	 * \brief Sango的默认数据验证器
	 */
	namespace DefaultValidators
	{
		/**
		 * \brief 不验证数据，直接返回true
		 * \param byteHead 数据
		 * \param byteCount 数据长度
		 * \return true
		 */
		bool NotValidateData(const uint8 *byteHead, uint32 byteCount);

		/**
		 * \brief 不验证数据，直接返回false
		 * \param dataHead 数据
		 * \param byteCount 数据长度
		 * \return false
		 */
		bool InvalidateAllData(const uint8 *dataHead, uint32 byteCount);

		/**
		 * \brief 计算CRC8值（默认该数组最后一位是CRC8的位置）
		 * \param byteHead 给定的数组的头
		 * \param byteCount 数组的字节数目
		 * \return CRC8值
		 */
		uint8 CalculateCRC8(const uint8 *byteHead, uint32 byteCount);

		/**
		 * \brief 计算并验证CRC8值（默认该数组最后一位是CRC8的位置）
		 * \param byteHead 给定的数组的头
		 * \param byteCount 数组的字节数目
		 * \return 是否通过验证
		 */
		bool ValidateCRC8(const uint8 *byteHead, uint32 byteCount);

		/**
		 * \brief 计算CRC16值（默认该数组最后两位是CRC16的位置）
		 * \param byteHead 给定的数组的头
		 * \param byteCount 数组的字节数目
		 * \return CRC16值
		 */
		uint16 CalculateCRC16(const uint8 *byteHead, uint32 byteCount);

		/**
		 * \brief 计算并验证CRC8值（默认该数组最后两位是CRC16的位置）
		 * \param byteHead 给定的数组的头
		 * \param byteCount 数组的字节数目
		 * \return 是否通过验证
		 */
		bool ValidateCRC16(const uint8 *byteHead, uint32 byteCount);
	}
}
