#pragma once

#include "MemoryTypes.hpp"

namespace WolfBlades
{

#pragma region class declaration

	/**
	 * \brief 代表循环队列
	 * \tparam TElement 循环队列的元素
	 * \Tparam DefaultElement 默认元素（也可以说代表空元素）
	 */
	template<typename TElement>
	class LoopQueue
	{
	protected:
		/**
		 * \brief 队列长度
		 */
		uint32 QueueSize;

		/**
		 * \brief 已经使用的空间
		 */
		volatile uint32 UsedSpace;

		/**
		 * \brief 队列的头指针
		 */
		TElement* QueueHead;

		/**
		 * \brief 指示这个在这个队列销毁的时候是否应该销毁队列的头指针
		 */
		bool CanDeleteQueueHead;

		/**
		 * \brief 右下标（Push函数使用的下标）
		 */
		uint32 RightIndex;

		/**
		 * \brief 左下标（Pop函数使用的下标）
		 */
		uint32 LeftIndex;

		/**
		 * \brief 指示当前是否正在移动右下标（如果正在移动，那么Push操作将不可用）
		 */
		volatile bool IsMovingRightIndex;

		/**
		 * \brief 指示当前是否正在移动左下标（如果正在移动，那么Pop操作将不可用）
		 */
		volatile bool IsMovingLeftIndex;

	public:
		/**
		 * \brief 按照循环队列的方式移动传入的下标，保证下标不会越界
		 * \param index 需要移动的下标
		 */
		void MoveIndex(uint32 &index) const;

		/**
		 * \brief 获取队列大小
		 * \return 队列大小
		 */
		uint32 GetQueueSize() const;

		/**
		 * \brief 获取已经使用的空间
		 * \return 已经使用的空间
		 */
		uint32 GetUsedSize() const;

		/**
		 * \brief 获取剩余的空间
		 * \return 剩余的空间
		 */
		uint32 GetSpaceLeft() const;

		/**
		 * \brief 获取队列是否为空
		 * \return 队列是否为空
		 */
		bool IsEmpty() const;

		/**
		 * \brief 获取队列是否装满
		 * \return 队列是否装满
		 */
		bool IsFull() const;

		/**
		 * \brief 获取队列的头指针
		 * \return 队列的头指针
		 */
		TElement* GetQueueHead() const;

		/**
		 * \brief 提供类似数组的方式访问队列中的元素
		 * \param index 下标
		 * \return 队列中相应的元素
		 */
		TElement &operator[](uint32 index) const;

		/**
		 * \brief 构造一个循环队列
		 * \param queueSize 指定队列的大小
		 */
		LoopQueue(uint32 queueSize);

		/**
		 * \brief 尝试销毁队列占用的空间
		 */
		~LoopQueue();

		/**
		 * \brief 把元素压入队列
		 * \param element 要压入的元素
		 * \return 是否成功压入队列
		 */
		bool Push(TElement element);

		/**
		 * \brief 调用Push压入元素（只在你确定这个队列不被其他线程占用的时候调用，即Push操作一定能成功才可）
		 * \param element 要压入的元素
		 * \return 当前实例，方便链式表达
		 */
		LoopQueue& operator<<(TElement element);

		/**
		 * \brief 把元素放出队列
		 * \param destination 放出元素的存放位置
		 * \return 是否成功放出一个元素
		 */
		bool Pop(TElement &destination);

		/**
		 * \brief 调用Pop放出元素
		 * \param destination 放出元素的目标位置
		 * \return 当前实例，方便链式表达
		 */
		LoopQueue& operator>>(TElement &destination);

		/**
		 * \brief 清理队列中已经使用的元素
		 * \param waitForOtherAction 是否等待其他行为执行完毕 
		 */
		void Clear(bool waitForOtherAction = false);

		/**
		 * \brief 访问元素
		 * \param index 从队列头开始的下标
		 * \return 目标元素
		 */
		TElement operator[](uint32 index);
	};

#pragma endregion

#pragma region member function implementation

	template <typename TElement>
	void LoopQueue<TElement>::MoveIndex(uint32& index) const
	{
		index = (index + 1) % QueueSize;
	}

	template <typename TElement>
	uint32 LoopQueue<TElement>::GetQueueSize() const
	{
		return QueueSize;
	}

	template <typename TElement>
	uint32 LoopQueue<TElement>::GetUsedSize() const
	{
		return UsedSpace;
	}

	template <typename TElement>
	uint32 LoopQueue<TElement>::GetSpaceLeft() const
	{
		return QueueSize - UsedSpace;
	}

	template <typename TElement>
	bool LoopQueue<TElement>::IsEmpty() const
	{
		return UsedSpace == 0;
	}

	template <typename TElement>
	bool LoopQueue<TElement>::IsFull() const
	{
		return UsedSpace == QueueSize;
	}

	template <typename TElement>
	TElement* LoopQueue<TElement>::GetQueueHead() const
	{
		return QueueHead;
	}

	template <typename TElement>
	TElement& LoopQueue<TElement>::operator[](const uint32 index) const
	{
		return QueueHead[(LeftIndex + index) % QueueSize];
	}

	template <typename TElement>
	LoopQueue<TElement>::LoopQueue(const uint32 queueSize)
	{
		QueueSize = queueSize;
		if (QueueSize == 0)
		{
			CanDeleteQueueHead = false;
			QueueHead = nullptr;
		}
		else
		{
			CanDeleteQueueHead = true;
			QueueHead = new TElement[QueueSize]();
		}

		LeftIndex = 0;
		RightIndex = 0;
		UsedSpace = 0;
		IsMovingLeftIndex = false;
		IsMovingRightIndex = false;
	}

	template <typename TElement>
	LoopQueue<TElement>::~LoopQueue()
	{
		if (CanDeleteQueueHead)
		{
			delete[] QueueHead;
		}
	}

	template <typename TElement>
	bool LoopQueue<TElement>::Push(TElement element)
	{
		if (IsMovingRightIndex || IsFull())
		{
			return false;
		}

		IsMovingRightIndex = true;
		QueueHead[RightIndex] = element;
		MoveIndex(RightIndex);
		UsedSpace++;
		IsMovingRightIndex = false;

		return true;
	}

	template <typename TElement>
	LoopQueue<TElement>& LoopQueue<TElement>::operator<<(TElement element)
	{
		Push(element);
		return *this;
	}

	template <typename TElement>
	bool LoopQueue<TElement>::Pop(TElement& destination)
	{
		if (IsMovingLeftIndex || IsEmpty())
		{
			return false;
		}

		IsMovingLeftIndex = true;
		destination = QueueHead[LeftIndex];
		MoveIndex(LeftIndex);
		UsedSpace--;
		IsMovingLeftIndex = false;

		return true;
	}

	template <typename TElement>
	LoopQueue<TElement>& LoopQueue<TElement>::operator>>(TElement& destination)
	{
		Pop(destination);
		return *this;
	}

	template <typename TElement>
	void LoopQueue<TElement>::Clear(const bool waitForOtherAction)
	{
		if (waitForOtherAction)
		{
			while (IsMovingLeftIndex || IsMovingRightIndex);
		}

		IsMovingLeftIndex = true;
		IsMovingRightIndex = true;
		LeftIndex = 0;
		RightIndex = 0;
		UsedSpace = 0;
		IsMovingLeftIndex = false;
		IsMovingRightIndex = false;
	}

	template <typename TElement>
	TElement LoopQueue<TElement>::operator[](const uint32 index)
	{
		return QueueHead[(LeftIndex + index) % QueueSize];
	}

#pragma endregion

}
