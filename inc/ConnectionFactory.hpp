#pragma once

#include "BufferedByteQueue.hpp"
#include "BytePackageProcessor.hpp"
#include "IOConnector.hpp"
#include "CommonMultiThread.hpp"

namespace WolfBlades
{
	/**
	 * \brief 连接工厂，提供简单的传输数据的方法
	 */
	class ConnectionFactory
	{
		/**
		 * \brief 接收时存储字节的队列
		 */
		BufferedByteQueue ReceiveBytesBuffer;

		/**
		 * \brief 发送包裹时的队列
		 */
		BufferedByteQueue::BytePackageQueue SendQueue;

		/**
		 * \brief 读取数据的缓冲包裹
		 */
		BytePackage ReadBuffer;

		/**
		 * \brief 写入数据的缓冲包裹
		 */
		BytePackage WriteBuffer;

		/**
		 * \brief 放出数据的缓冲包裹
		 */
		BytePackage PopBuffer;

		/**
		 * \brief 连接器
		 */
		const IOConnector* Connector;

		/**
		 * \brief 包装器
		 */
		BytePackageProcessor Wrapper;

		/**
		 * \brief 拆包器
		 */
		BytePackageProcessor Extractor;

		/**
		 * \brief 验证器
		 */
		DataValidator Validator;

		/**
		 * \brief 要发送包裹的大小
		 */
		uint32 SizeofSendPackage;

		/**
		 * \brief 要接收包裹的大小
		 */
		uint32 SizeofReceivePackage;

	public:

		ConnectionFactory(const IOConnector* connector, BytePackageProcessor wrapper, BytePackageProcessor extractor, DataValidator validator, uint32 sizeofSendPackage, uint32 sizeofReceivePackage);

		/**
		 * \brief 发送结构体包裹
		 * \param source 要发送的数据
		 * \return 是否成功发送
		 */
		bool Send(const BytePackage& source);

		/**
		 * \brief 放出的结构体包裹
		 * \param destination 放出的结构体要存储的位置 
		 * \return 是否成功放出
		 */
		bool Pop(const BytePackage& destination);

		/**
		 * \brief 工厂自己发送和接受包裹的循环进程
		 * \param shutdownFlag 关闭工厂的信号
		 */
		void DoRoutine(bool *shutdownFlag);
	};
}
